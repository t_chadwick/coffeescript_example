var gulp = require('gulp');
var sass = require('gulp-sass');
var coffee = require('gulp-coffee');
var gutil = require('gulp-util');
//SASS
gulp.task('styles', function() {
  gulp.src('assets/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./assets/css/'))
});
// CoffeeScript
gulp.task('js', function() {
  gulp.src('assets/coffee/**/*.coffee')
    .pipe(coffee({bare: true}).on('error', gutil.log))
    .pipe(gulp.dest('./assets/js/'))
});

// Watch Files & Automatically Update CSS & JS
gulp.task('default', function(){
  gulp.watch('assets/sass/**/*.scss',['styles']);
  gulp.watch('assets/coffee/**/*.coffee',['js']);
});

// Can now just run 'gulp' in konsole to compile sass and coffee script
