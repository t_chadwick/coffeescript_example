###
This Script uses JQUERY
###
#Function
addTitle = ->
  $('h1').append('Hello World')

#Call Function upon Creation
do addMoreText = ->
  $('h4').append('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.')

#Data Handling
data =
  name: "Tom Chadwick"
  numbers: [1,2,3,4]
  links:
    portfolio: "www.tchadwickdev.co.uk"
    google: "www.google.co.uk"

#Calling
addTitle()

#Regular JS
`console.log('This is regular JS compiled from CoffeeScript')`
